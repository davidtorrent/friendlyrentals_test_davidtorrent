﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    public class Kata
    {
        private string _delimiter;
        private int[] _numbers;     

        public int Add(string numbers)
        {
            //Empty string provided, return 0
            if (String.IsNullOrEmpty(numbers)) return 0;
            //String is not empty, parse and get delimiter/numbers
            this.parseAndGetDelimiterAndNumbers(numbers);
            //Once the delimiter and number is extracted validate that we have it
            if (string.IsNullOrEmpty(this._delimiter) || this._numbers == null) throw new FormatException("String is not following the pattern");

            int[] negativeNumbers = this._numbers.Where(i => i < 0).ToArray();
            if (negativeNumbers.Length > 0) throw new InvalidOperationException("negatives not allowed " + string.Join("", negativeNumbers));

            //Validations are ok at this point, proceed to sum
            return (this._numbers.Sum());
        }

        /// <summary>
        /// Parse the string and get the delimiter and the numbers in a int[]
        /// </summary>
        /// <param name="numbers"></param>

        private void parseAndGetDelimiterAndNumbers(string numbers)
        {
           Regex regexDelimiter = new Regex(@"^\/\/(.*?)[\n]");
            Match match = regexDelimiter.Match(numbers); //,\n1,2,3,4 */
            if (!match.Success)
            {
                return;
            }
            else
            {
                //If got the delimiter continue checking the numberr
                this._delimiter = match.Groups[1].ToString();
                Regex regexFullNumbers = new Regex(@"[\n].*");   /*  1,2,3,4  */
                match = regexFullNumbers.Match(numbers);
                if (match.Success)
                {
                    try
                    {
                        //Replace the \n then split the string and finally convert it to an int array
                        string numbersInString = match.Groups[0].ToString().Replace("\n", string.Empty);
                        this._numbers = numbersInString.Split(Convert.ToChar(_delimiter)).Select(n => Convert.ToInt32(n)).ToArray();
                    }
                    catch (System.FormatException)
                    {
                        // If we got this exception is because the delimiter is not in the numbers part so
                        // we cannot split and convert to array
                        this._numbers = null;
                    }
                }
            }
        }
    }
}
