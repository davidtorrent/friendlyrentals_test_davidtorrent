﻿using System;
using fr_stringcalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace fr_stringcalculator_test
{
    [TestClass]
    public class frTests
    {
        [TestMethod]
        public void when_no_input_parameters()
        {
            Kata calc = new Kata();
            Assert.AreEqual(calc.Add(string.Empty), 0);
        }

        [TestMethod]
        public void when_sum_is_as_expected_and_is_6_and_delimiter_is_comma()
        {
            Kata calc = new Kata();
            Assert.AreEqual(calc.Add("//,\n1,2,3"), 6);
        }

        [TestMethod]
        public void when_sum_is_as_expected_and_is_6_and_delimiter_is_asterisc()
        {
            Kata calc = new Kata();
            Assert.AreEqual(calc.Add("//*\n1*2*3"), 6);
        }

        [TestMethod]
        public void when_the_string_is_bad_constructed_with_a_deliminter_not_used()
        {
            try
            {
                Kata calc = new Kata();
                calc.Add("//;\n2,4,6");
                Assert.Fail("We should have seen an exception");
            }
            catch (FormatException ae)
            {
                Assert.AreEqual("String is not following the pattern", ae.Message);
            }
        }


        [TestMethod]
        public void when_the_string_has_negative_numbers()
        {
            try
            {
                Kata calc = new Kata();
                calc.Add("//;\n-1;-2;-3");
                Assert.Fail("We should have seen an exception because we have negative numbers");
            }
            catch (InvalidOperationException ae)
            {
                StringAssert.Contains(ae.Message, "negatives not allowed ");
            }
        }
    }
}
